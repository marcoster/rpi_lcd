	/*
	Example Programm for using the Arduino LiquidCrystal Library on the Raspberry Pi
    Copyright (C) 2012  Marco Sterbik

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	*/
	
	/*
	All credits for the following libraries/files goes to the Arduino developers:
		LiquidCrystal.cpp/.h
		new.cpp/.h
		Print.cpp/.h
		Printable.h
		WString.cpp/.h
	I just made minor changings (unincluding arduino.h and including my_stdlib.h in one file)
	Visit arduino.cc for more informations about their awesome work.
	
	All credits for the wiringPi library go to Grodon, visit:
	https://projects.drogon.net/raspberry-pi/wiringpi/
	*/
#include "LiquidCrystal.h"
#include <unistd.h>
#include <stdlib.h>

#include "wiringPi.h"

int main(void)
{
	unsigned int i;
	unsigned char c;
	
	//Initializing the wiringPi GPIO library
	//--> i use "WPI_MODE_GPIO" so the GPIO numbers of the rpi are used
	wiringPiSetup();
	wiringPiGpioMode(WPI_MODE_GPIO);
		
	/************************************
	Connection of the display:
		Display					 Rpi
		  Pin					 GPIO
		Number	Description		Number
		  1			GND			GND
		  2			VDD			+5V
		  3			Vo			Connected to a potentiometer
		  4			RS			GPIO 0
		  5			R/W			GPIO 4
		  6			E			GPIO 1
		  7			D0
		  8			D1
		  9			D2
		 10			D3
		 11			D4			GPIO 21
		 12			D5			GPIO 22
		 13			D6			GPIO 23
		 14			D7			GPIO 24
		 15		  LED+			+5V
		 16		  LED-			GND
	
	The GPIOs above are just used in this example.
	All GPIOs can be used to drive a display!
	************************************/
	
	//initializing the pins
	LiquidCrystal lcd(0, 4, 1, 21, 22, 23, 24);
	lcd.begin(20, 4);
	
	//just printing something onto the lcd
	lcd.print("What's up?");
	lcd.setCursor(0, 1);
	lcd.print("Git rulez!");
	lcd.setCursor(0, 2);
	lcd.print("Hello Raspberry Pi!");
	

	while(1)
	{
		//printing the time in seconds since the program started into line 4
		lcd.setCursor(0, 3);
		char ledbuffer[20];
		sprintf(ledbuffer, "Seconds: %u", i);
		lcd.print(ledbuffer);
		for(c = 0; c < 100; c++)
		{
			usleep(10000);
		}
		i++;
	}	
	return 0;
}
