CC=g++

OBJ = main.o LiquidCrystal.o wiringPi.o Print.o

main: $(OBJ)

clean:
	rm -f main $(OBJ)
